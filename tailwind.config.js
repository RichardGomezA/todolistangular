/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/index.html', './src/app/pages/home/home.component.html', './src/app/components/navbar/navbar.component.html', './src/app/components/footer/footer.component.html'],
  theme: {
    extend: {},
  },
  plugins: [],
  theme: {
    fontFamily: {
      'script': ['luxurious script'],
      'roboto': ['roboto'],
    }
  }
}
