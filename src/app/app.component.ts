import { Component, Input } from '@angular/core';
import { DataService } from './services/data.service';
import { NavbarComponent } from './components/navbar/navbar.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angularPrueba';

  constructor() {
  }

  ngOnInit(): void {
  }

}
