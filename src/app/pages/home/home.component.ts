import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/services/todo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  todos : Todo[] = [];
  newTodo : string;
  newData : Date;
  newState : string;

  constructor() { }
  ngOnInit(): void {
  }

  adicionarTarea() {
    if(this.newTodo){
      let todo = new Todo();
      todo.descripcion = this.newTodo;
      todo.date = this.newData;
      todo.estado = this.newState;
      todo.isVisible = true;
      this.todos.push(todo);
      this.newTodo = '';
    }else{
        alert('Por favor ingrese una tarea')
    }

  }

  remove(id:number){
    this.todos = this.todos.filter((v,i)=> i !== id);
  }

}
