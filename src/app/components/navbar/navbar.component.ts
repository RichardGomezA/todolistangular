import { outputAst } from '@angular/compiler';
import { Component, OnInit, Output, Input } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
  selector: boolean = false;
  selectorDropdown() {
    this.selector = !this.selector;
  }
  selected: string = "Collections";
  isToggle: number = 1;

  dataList: any = {};
  name: any = {};
  img: any = {};

  constructor(private dataservice: DataService){
  }

  ngOnInit(): void {

    this.dataservice.getData().subscribe((response) => {
      this.dataList = response;
      const results = this.dataList.results[0];
      this.name = results.name;
      this.img = results.picture;
    });
  }

}
